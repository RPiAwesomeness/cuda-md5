#include <cstdint>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

// Precalculated sine values
static const uint32_t sinVals[64] = {
  0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
  0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
  0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
  0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
  0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
  0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
  0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
  0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
  0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
  0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
  0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
  0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
  0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
  0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
  0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
  0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
};

// Magic values for initialization
static const uint32_t A0 = 0x67452301;
static const uint32_t B0 = 0xefcdab89;
static const uint32_t C0 = 0x98badcfe;
static const uint32_t D0 = 0x10325476;

inline uint32_t F(uint32_t x, uint32_t y, uint32_t z) {
  return ((x & y) | (~x & z));
}

inline uint32_t G(uint32_t x, uint32_t y, uint32_t z) {
  return ((x & z) | (y & ~z));
}

inline uint32_t H(uint32_t x, uint32_t y, uint32_t z) {
  return (x ^ y ^ z);
}

inline uint32_t I(uint32_t x, uint32_t y, uint32_t z) {
  return (y ^ (x | ~z));
}

// Logical wrapping left shift handling
inline uint32_t shift(uint32_t x, uint32_t s) {
  return ((x << s) | (x >> (32 - s)));
}

// Round 1 handler
inline void FF(uint32_t &a, uint32_t b, uint32_t c, uint32_t d, uint32_t x, uint32_t s, uint32_t t) {
  a += F(b, c, d) + x + t;
  a = shift(a, s) + b;
}

// Round 2 handler
inline void GG(uint32_t &a, uint32_t b, uint32_t c, uint32_t d, uint32_t x, uint32_t s, uint32_t t) {
  a += G(b, c, d) + x + t;
  a = shift(a, s) + b;
}

// Round 3 handler
inline void HH(uint32_t &a, uint32_t b, uint32_t c, uint32_t d, uint32_t x, uint32_t s, uint32_t t) {
  a += H(b, c, d) + x + t;
  a = shift(a, s) + b;
}

// Round 4 handler
inline void II(uint32_t &a, uint32_t b, uint32_t c, uint32_t d, uint32_t x, uint32_t s, uint32_t t) {
  a += I(b, c, d) + x + t;
  a = shift(a, s) + b;
}

// Provides binary representation of unsigned char
string charToBin(u_char val) {
  string mask;
  for (int i = 0; i < (8 * sizeof(u_char)); i++) {
    mask.push_back((val << i) & (1 << (8 * sizeof(u_char) - 1)) ? '1' : '0');
  }

  return mask;
}

// Handles padding of source string to proper 512 bit length
void md5Pad(uint32_t *dest, char *src, uint64_t len) {
  // Number of characters in buffer * 8 mod 64
  uint64_t newLen;
  for (newLen = len * 8 + 1; newLen % 512 != 448; newLen++);
  newLen /= 8;

  // Initialize result buffer with null characters
  u_char buf[64] = {0};

  // Copy bytes from characters into uint memory space, no conversion necessary
  memcpy(buf, src, len);

  // Append terminating char
  buf[len] = 0x80;

  // Append length to buffer
  uint64_t bitsLen = len * 8;
  memcpy(buf + newLen, &bitsLen, 4);

  // Copy character buffer to destination buffer after zeroing contents
  memset(dest, 0, 16);
  memcpy(dest, buf, 64);

  return;
}

// Handles conversion of unsigned int value (from result digest) to hex string
// in little endian order
string intToHexReverseEndian(uint32_t val) {
  ostringstream ss;
  ss << hex << setfill('0') << setw(2) << ((val >>  0) & 0xFF);
  ss << hex << setfill('0') << setw(2) << ((val >>  8) & 0xFF);
  ss << hex << setfill('0') << setw(2) << ((val >> 16) & 0xFF);
  ss << hex << setfill('0') << setw(2) << ((val >> 24) & 0xFF);

  return ss.str();
}

// Handles actual MD5 hashing when provided with properly padded int array
void MD5(uint32_t *outBuf, uint32_t *inBuf) {
  uint32_t a, b, c, d;

  const uint32_t a0 = 0x67452301;
  const uint32_t b0 = 0xEFCDAB89;
  const uint32_t c0 = 0x98BADCFE;
  const uint32_t d0 = 0x10325476;

  a = a0;
  b = b0;
  c = c0;
  d = d0;

  // Shift amounts 1st round
  static const u_char S11 =  7;
  static const u_char S12 = 12;
  static const u_char S13 = 17;
  static const u_char S14 = 22;

  FF ( a, b, c, d, inBuf[ 0], S11, sinVals[ 0]); /* 1 */
  FF ( d, a, b, c, inBuf[ 1], S12, sinVals[ 1]); /* 2 */
  FF ( c, d, a, b, inBuf[ 2], S13, sinVals[ 2]); /* 3 */
  FF ( b, c, d, a, inBuf[ 3], S14, sinVals[ 3]); /* 4 */

  FF ( a, b, c, d, inBuf[ 4], S11, sinVals[ 4]); /* 5 */
  FF ( d, a, b, c, inBuf[ 5], S12, sinVals[ 5]); /* 6 */
  FF ( c, d, a, b, inBuf[ 6], S13, sinVals[ 6]); /* 7 */
  FF ( b, c, d, a, inBuf[ 7], S14, sinVals[ 7]); /* 8 */

  FF ( a, b, c, d, inBuf[ 8], S11, sinVals[ 8]); /* 9 */
  FF ( d, a, b, c, inBuf[ 9], S12, sinVals[ 9]); /* 10 */
  FF ( c, d, a, b, inBuf[10], S13, sinVals[10]); /* 11 */
  FF ( b, c, d, a, inBuf[11], S14, sinVals[11]); /* 12 */ 

  FF ( a, b, c, d, inBuf[12], S11, sinVals[12]); /* 13 */
  FF ( d, a, b, c, inBuf[13], S12, sinVals[13]); /* 14 */
  FF ( c, d, a, b, inBuf[14], S13, sinVals[14]); /* 15 */
  FF ( b, c, d, a, inBuf[15], S14, sinVals[15]); /* 16 */

  // Shift amounts 2nd round
  static const u_char S21 =  5;
  static const u_char S22 =  9;
  static const u_char S23 = 14;
  static const u_char S24 = 20;

  GG ( a, b, c, d, inBuf[ 1], S21, sinVals[16]); /* 17 */
  GG ( d, a, b, c, inBuf[ 6], S22, sinVals[17]); /* 18 */
  GG ( c, d, a, b, inBuf[11], S23, sinVals[18]); /* 19 */
  GG ( b, c, d, a, inBuf[ 0], S24, sinVals[19]); /* 20 */

  GG ( a, b, c, d, inBuf[ 5], S21, sinVals[20]); /* 21 */
  GG ( d, a, b, c, inBuf[10], S22, sinVals[21]); /* 22 */
  GG ( c, d, a, b, inBuf[15], S23, sinVals[22]); /* 23 */
  GG ( b, c, d, a, inBuf[ 4], S24, sinVals[23]); /* 24 */

  GG ( a, b, c, d, inBuf[ 9], S21, sinVals[24]); /* 25 */
  GG ( d, a, b, c, inBuf[14], S22, sinVals[25]); /* 26 */
  GG ( c, d, a, b, inBuf[ 3], S23, sinVals[26]); /* 27 */
  GG ( b, c, d, a, inBuf[ 8], S24, sinVals[27]); /* 28 */

  GG ( a, b, c, d, inBuf[13], S21, sinVals[28]); /* 29 */
  GG ( d, a, b, c, inBuf[ 2], S22, sinVals[29]); /* 30 */
  GG ( c, d, a, b, inBuf[ 7], S23, sinVals[30]); /* 31 */
  GG ( b, c, d, a, inBuf[12], S24, sinVals[31]); /* 32 */

  // Shift amounts 3rd round
  static const u_char S31 =  4;
  static const u_char S32 = 11;
  static const u_char S33 = 16;
  static const u_char S34 = 23;

  HH ( a, b, c, d, inBuf[ 5], S31, sinVals[32]); /* 33 */
  HH ( d, a, b, c, inBuf[ 8], S32, sinVals[33]); /* 34 */
  HH ( c, d, a, b, inBuf[11], S33, sinVals[34]); /* 35 */
  HH ( b, c, d, a, inBuf[14], S34, sinVals[35]); /* 36 */

  HH ( a, b, c, d, inBuf[ 1], S31, sinVals[36]); /* 37 */
  HH ( d, a, b, c, inBuf[ 4], S32, sinVals[37]); /* 38 */
  HH ( c, d, a, b, inBuf[ 7], S33, sinVals[38]); /* 39 */
  HH ( b, c, d, a, inBuf[10], S34, sinVals[39]); /* 40 */

  HH ( a, b, c, d, inBuf[13], S31, sinVals[40]); /* 41 */
  HH ( d, a, b, c, inBuf[ 0], S32, sinVals[41]); /* 42 */
  HH ( c, d, a, b, inBuf[ 3], S33, sinVals[42]); /* 43 */
  HH ( b, c, d, a, inBuf[ 6], S34, sinVals[43]); /* 44 */

  HH ( a, b, c, d, inBuf[ 9], S31, sinVals[44]); /* 45 */
  HH ( d, a, b, c, inBuf[12], S32, sinVals[45]); /* 46 */
  HH ( c, d, a, b, inBuf[15], S33, sinVals[46]); /* 47 */
  HH ( b, c, d, a, inBuf[ 2], S34, sinVals[47]); /* 48 */

  // Shift amounts 4th round
  static const u_char S41 =  6;
  static const u_char S42 = 10;
  static const u_char S43 = 15;
  static const u_char S44 = 21;

  II ( a, b, c, d, inBuf[ 0], S41, sinVals[48]); /* 49 */
  II ( d, a, b, c, inBuf[ 7], S42, sinVals[49]); /* 50 */
  II ( c, d, a, b, inBuf[14], S43, sinVals[50]); /* 51 */
  II ( b, c, d, a, inBuf[ 5], S44, sinVals[51]); /* 52 */

  II ( a, b, c, d, inBuf[12], S41, sinVals[52]); /* 53 */
  II ( d, a, b, c, inBuf[ 3], S42, sinVals[53]); /* 54 */
  II ( c, d, a, b, inBuf[10], S43, sinVals[54]); /* 55 */
  II ( b, c, d, a, inBuf[ 1], S44, sinVals[55]); /* 56 */

  II ( a, b, c, d, inBuf[ 8], S41, sinVals[56]); /* 57 */
  II ( d, a, b, c, inBuf[15], S42, sinVals[57]); /* 58 */
  II ( c, d, a, b, inBuf[ 6], S43, sinVals[58]); /* 59 */
  II ( b, c, d, a, inBuf[13], S44, sinVals[59]); /* 60 */

  II ( a, b, c, d, inBuf[ 4], S41, sinVals[60]); /* 61 */
  II ( d, a, b, c, inBuf[11], S42, sinVals[61]); /* 62 */
  II ( c, d, a, b, inBuf[ 2], S43, sinVals[62]); /* 63 */
  II ( b, c, d, a, inBuf[ 9], S44, sinVals[63]); /* 64 */

  // Re-add initialization magic values
	a += a0;
	b += b0;
	c += c0;
	d += d0;

	outBuf[0] = a;
	outBuf[1] = b;
	outBuf[2] = c;
	outBuf[3] = d;

  return;
}

int main() {
  // Input string
  string passwordStr = "";

  // Get input from user
  cout << "Enter string to hash: ";
  getline(cin, passwordStr);

  // This implementation assumes the
  cout << passwordStr.length() * 8 << endl;

  // Pad password
  uint32_t padded[16] = {0};
  md5Pad(padded, &passwordStr[0], passwordStr.length());

  // Perform MD5 hash on padded password int values
  uint32_t digest[4] = {0};
	MD5(digest, padded);

  cout << "Digest: " 
    << intToHexReverseEndian(digest[0])
    << intToHexReverseEndian(digest[1])
    << intToHexReverseEndian(digest[2])
    << intToHexReverseEndian(digest[3])
    << endl;

  return 0;
}