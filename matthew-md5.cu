/* MD5
Original algorithm by RSA Data Security, Inc
Adapted for NVIDIA CUDA by Matthew McClaskey
 
Copyright (C) 1991-2, RSA Data Security, Inc. Created 1991. All
rights reserved.
 
License to copy and use this software is granted provided that it
is identified as the "RSA Data Security, Inc. MD5 Message-Digest
Algorithm" in all material mentioning or referencing this software
or this function.
 
License is also granted to make and use derivative works provided
that such works are identified as "derived from the RSA Data
Security, Inc. MD5 Message-Digest Algorithm" in all material
mentioning or referencing the derived work.
 
RSA Data Security, Inc. makes no representations concerning either
the merchantability of this software or the suitability of this
software for any particular purpose. It is provided "as is"
without express or implied warranty of any kind.
 
These notices must be retained in any copies of any part of this
documentation and/or software.
*/
 
const unsigned int S11 = 7;
const unsigned int S12 = 12;
const unsigned int S13 = 17;
const unsigned int S14 = 22;
const unsigned int S21 = 5;
const unsigned int S22 = 9;
const unsigned int S23 = 14;
const unsigned int S24 = 20;
const unsigned int S31 = 4;
const unsigned int S32 = 11;
const unsigned int S33 = 16;
const unsigned int S34 = 23;
const unsigned int S41 = 6;
const unsigned int S42 = 10;
const unsigned int S43 = 15;
const unsigned int S44 = 21;
 
const int blocksize = 4; //<--MD5 Block size (in 32bit integers)
const int NUMTHREADS = 95; //<--NUMBER OF THREADS PER BLOCK
const unsigned int pwdbitlen = 40; //<--number of bits in plain text
 
/* F, G, H and I are basic MD5 functions */
__device__ inline unsigned int F(unsigned int x, unsigned int y, unsigned int z) { return (((x) & (y)) | ((~x) & (z))); }
__device__ inline unsigned int G(unsigned int x, unsigned int y, unsigned int z) { return (((x) & (z)) | ((y) & (~z))); }
__device__ inline unsigned int H(unsigned int x, unsigned int y, unsigned int z) { return ((x) ^ (y) ^ (z)); }
__device__ inline unsigned int I(unsigned int x, unsigned int y, unsigned int z) { return ((y) ^ ((x) | (~z))); }
 
/* ROTATE_LEFT rotates x left n bits */
#define ROTATE_LEFT(x, n) (((x) << (n)) | ((x) >> (32-(n))))
 
/* Rotation is separate from addition to prevent recomputation */
__device__ inline void FF(unsigned int &a, unsigned int b, unsigned int c, unsigned int d, unsigned int x, unsigned int s, unsigned int ac)
{
	a = ROTATE_LEFT(a + F(b, c, d) + x + ac, s) + b;
}
 
__device__ inline void GG(unsigned int &a, unsigned int b, unsigned int c, unsigned int d, unsigned int x, unsigned int s, unsigned int ac)
{
	a = ROTATE_LEFT(a + G(b, c, d) + x + ac, s) + b;
}
 
__device__ inline void HH(unsigned int &a, unsigned int b, unsigned int c, unsigned int d, unsigned int x, unsigned int s, unsigned int ac)
{
	a = ROTATE_LEFT(a + H(b ,c ,d) + x + ac, s) + b;
}
 
__device__ inline void II(unsigned int &a, unsigned int b, unsigned int c, unsigned int d, unsigned int x, unsigned int s, unsigned int ac)
{
	a = ROTATE_LEFT(a + I(b, c, d) + x + ac, s) + b;
}
 
__device__ void encode(unsigned char output[], const unsigned int input[], unsigned int len)
{
    for (unsigned int i = 0, j = 0; j < len; i++, j += 4)
    {
        output[j] = (unsigned char)(input[i] & 0xff);
        output[j + 1] = (unsigned char)((input[i] >> 8) & 0xff);
        output[j + 2] = (unsigned char)((input[i] >> 16) & 0xff);
        output[j + 3] = (unsigned char)((input[i] >> 24) & 0xff);
    }
}
 
extern "C" __global__ void Parrallel_Hash(unsigned int *input, char *output)
{  
	unsigned int a, b, c, d;

	//plain text int array
	unsigned int x[3];
	unsigned int total = 32;
	total += threadIdx.x;
   
	//Block Dim 9025 x 1
	total += ((blockIdx.x / 95) + 32) * 256;
	total += ((blockIdx.x % 95) + 32) * 65536;
   
	//Block Dim 95 x 95
	//total += (blockIdx.x + 32) * 256;
	//total += (blockIdx.y + 32) * 65536;
   
	//set ending integer of buffer
	//x[1] = input[4]; //up to 8 char password, last 4 char set by host input
	//x[1] = 128; //4 char password
	x[2] = 0;
               
	//loop sets 4th char
	for (unsigned int t = 32; t < 127; t++)
	{
		x[0] = total + (t * 16777216);
               
		//loop sets 5th char
		for (unsigned int u = 32; u < 127; u++)
		{
			x[1] = u + input[4]; //sets 5th through 8th char

			//load magic numbers
			a = 0x67452301;
			b = 0xefcdab89;
			c = 0x98badcfe;
			d = 0x10325476;

			// // Round 1
			FF ( a, b, c, d, x[ 0], S11, 3614090360); /* 1 */
			FF ( d, a, b, c, x[ 1], S12, 3905402710); /* 2 */
			FF ( c, d, a, b, x[ 2], S13,  606105819); /* 3 */
			FF ( b, c, d, a, 0, S14, 3250441966); /* 4 */
			FF ( a, b, c, d, 0, S11, 4118548399); /* 5 */
			FF ( d, a, b, c, 0, S12, 1200080426); /* 6 */
			FF ( c, d, a, b, 0, S13, 2821735955); /* 7 */
			FF ( b, c, d, a, 0, S14, 4249261313); /* 8 */
			FF ( a, b, c, d, 0, S11, 1770035416); /* 9 */
			FF ( d, a, b, c, 0, S12, 2336552879); /* 10 */
			FF ( c, d, a, b, 0, S13, 4294925233); /* 11 */
			FF ( b, c, d, a, 0, S14, 2304563134); /* 12 */
			FF ( a, b, c, d, 0, S11, 1804603682); /* 13 */
			FF ( d, a, b, c, 0, S12, 4254626195); /* 14 */
			FF ( c, d, a, b, pwdbitlen, S13, 2792965006); /* 15 */
			FF ( b, c, d, a, 0, S14, 1236535329); /* 16 */

			// Round 2
			GG (a, b, c, d, x[ 1], S21, 0xf61e2562); // 17
			GG (d, a, b, c, 0, S22, 0xc040b340); // 18
			GG (c, d, a, b, 0, S23, 0x265e5a51); // 19
			GG (b, c, d, a, x[ 0], S24, 0xe9b6c7aa); // 20
			GG (a, b, c, d, 0, S21, 0xd62f105d); // 21
			GG (d, a, b, c, 0, S22,  0x2441453); // 22
			GG (c, d, a, b, 0, S23, 0xd8a1e681); // 23
			GG (b, c, d, a, 0, S24, 0xe7d3fbc8); // 24
			GG (a, b, c, d, 0, S21, 0x21e1cde6); // 25
			GG (d, a, b, c, pwdbitlen, S22, 0xc33707d6); // 26
			GG (c, d, a, b, 0, S23, 0xf4d50d87); // 27
			GG (b, c, d, a, 0, S24, 0x455a14ed); // 28
			GG (a, b, c, d, 0, S21, 0xa9e3e905); // 29
			GG (d, a, b, c, x[ 2], S22, 0xfcefa3f8); // 30
			GG (c, d, a, b, 0, S23, 0x676f02d9); // 31
			GG (b, c, d, a, 0, S24, 0x8d2a4c8a); // 32

			// Round 3
			HH (a, b, c, d, 0, S31, 0xfffa3942); // 33
			HH (d, a, b, c, 0, S32, 0x8771f681); // 34
			HH (c, d, a, b, 0, S33, 0x6d9d6122); // 35
			HH (b, c, d, a, pwdbitlen, S34, 0xfde5380c); // 36
			HH (a, b, c, d, x[ 1], S31, 0xa4beea44); // 37
			HH (d, a, b, c, 0, S32, 0x4bdecfa9); // 38
			HH (c, d, a, b, 0, S33, 0xf6bb4b60); // 39
			HH (b, c, d, a, 0, S34, 0xbebfbc70); // 40
			HH (a, b, c, d, 0, S31, 0x289b7ec6); // 41
			HH (d, a, b, c, x[ 0], S32, 0xeaa127fa); // 42
			HH (c, d, a, b, 0, S33, 0xd4ef3085); // 43
			HH (b, c, d, a, 0, S34,  0x4881d05); // 44
			HH (a, b, c, d, 0, S31, 0xd9d4d039); // 45
			HH (d, a, b, c, 0, S32, 0xe6db99e5); // 46
			HH (c, d, a, b, 0, S33, 0x1fa27cf8); // 47
			HH (b, c, d, a, x[ 2], S34, 0xc4ac5665); // 48

			// Round 4
			II (a, b, c, d, x[ 0], S41, 0xf4292244); // 49
			II (d, a, b, c, 0, S42, 0x432aff97); // 50
			II (c, d, a, b, pwdbitlen, S43, 0xab9423a7); // 51
			II (b, c, d, a, 0, S44, 0xfc93a039); // 52
			II (a, b, c, d, 0, S41, 0x655b59c3); // 53
			II (d, a, b, c, 0, S42, 0x8f0ccc92); // 54
			II (c, d, a, b, 0, S43, 0xffeff47d); // 55
			II (b, c, d, a, x[ 1], S44, 0x85845dd1); // 56
			II (a, b, c, d, 0, S41, 0x6fa87e4f); // 57
			II (d, a, b, c, 0, S42, 0xfe2ce6e0); // 58
			II (c, d, a, b, 0, S43, 0xa3014314); // 59
			II (b, c, d, a, 0, S44, 0x4e0811a1); // 60
			II (a, b, c, d, 0, S41, 0xf7537e82); // 61
			II (d, a, b, c, 0, S42, 0xbd3af235); // 62
			II (c, d, a, b, x[ 2], S43, 0x2ad7d2bb); // 63
			II (b, c, d, a, 0, S44, 0xeb86d391); // 64

			a += 0x67452301;
			b += 0xefcdab89;
			c += 0x98badcfe;
			d += 0x10325476;

			//check if hash matches
			if (a == input[0] &&
					b == input[1] &&
					c == input[2] &&
					d == input[3])
					{
							unsigned char firstInt[8];
							encode(&firstInt[0], &x[0], 8);

							*output = firstInt[0];
							*(output + 1) = firstInt[1];
							*(output + 2) = firstInt[2];
							*(output + 3) = firstInt[3];
							*(output + 4) = firstInt[4];
							*(output + 5) = firstInt[5];
							*(output + 6) = firstInt[6];
							*(output + 7) = firstInt[7];
					}
		}
	}
}